package com.androiddevs.mvvmnewsapp.db

import android.content.Context
import androidx.room.*
import com.androiddevs.mvvmnewsapp.models.Article

@Database(
    entities = [Article::class],
    version = 1
)

// database room class needs to be abstract
// type converters = Room can only handle primitive data types not custom classes
// type converters helps converts those custom classes to primitive types and vice-versa
@TypeConverters(Converters::class)
abstract class ArticleDatabase : RoomDatabase(){

    abstract fun getArticleDao(): ArticleDao

    // for singleton class instance
    companion object {
        // volatile = other threads can see if it changes instance
        @Volatile
        private var instance: ArticleDatabase? = null
        private val LOCK = Any()

        // called when instance of ArticleDatabase() is called
        // synchronized = cannot be accessed by other thread at same time
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: createDatabase(context).also{ instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                ArticleDatabase::class.java,
                "article_db.db"
            ).build()

    }
}